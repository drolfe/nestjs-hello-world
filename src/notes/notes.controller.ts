import { Get, Controller } from '@nestjs/common';
import { AppService } from '../app.service';

@Controller('notes')
export class NotesController {
  constructor(private readonly appService: AppService) {}
  
  @Get('')
  root(): string {
    return this.appService.root(); 
  }
}
